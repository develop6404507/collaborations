# L'Épopée de Codeville : Une Aventure GitLab

## Chapitre 1 : La Cartographie des Jalons

**Q : Pouvez-vous expliquer les jalons et la procédure pour les consulter ?**

---

## Chapitre 2 : Les Sceaux des Labels

**Q : Pouvez-vous expliquer les labels et procédure pour les consulter ?**

---

## Chapitre 3 : Le Grimoire du Wiki

**Q : Pouvez-vous expliquer le Wiki et procédure pour les consulter ?**

---

## Chapitre 4 : Les Parchemins des Tickets

**Q : Pouvez-vous expliquer les tickets et procédure pour les consulter ?**


---

## Chapitre 5 : Le Tableau des Quêtes

**Q : Comment accède-t-on au Tableau des tickets ?**


---

## Chapitre 6 : Les Jalons, Gardiens des Tickets

**Q : Pouvez-vous expliquer le rôle des jalons sur un ticket ?**

---

## Chapitre 7 : Les Labels, Messagers des Tickets

**Q : Pouvez-vous expliquer le rôle des labels sur un ticket ?**

---

## Chapitre 8 : L'Alchimie du Code 

**Q : Expliquez la différence entre le *commit*, le *pull*, le *push* et les *branches* ?**

---
## Chapitre 9 : L'Alchimie du Code suite

**Q : Expliquez la différence entre le *merge* et *rebase* ?**

---

## Chapitre 10 : La Conquête des Tickets

**Q : Pouvez-vous lister les différentes étapes à suivre dans la résolution d'un ticket ?**

